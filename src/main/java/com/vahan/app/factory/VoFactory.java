package com.vahan.app.factory;

import com.vahan.app.comparator.FileComparator;
import com.vahan.app.vo.File;

public class VoFactory {

    public File getFile(String name, Integer length, Integer start, Integer end)  {
        return new File(name, length, start, end);
    };

    public FileComparator getFileComparator() {
        return new FileComparator();
    }
}
