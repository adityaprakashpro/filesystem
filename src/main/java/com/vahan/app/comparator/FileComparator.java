package com.vahan.app.comparator;

import com.vahan.app.vo.File;

import java.util.Comparator;

public class FileComparator implements Comparator<File> {
    @Override
    public int compare(File a, File b) {
        return a.getStartIndex() - b.getStartIndex();
    }
}

