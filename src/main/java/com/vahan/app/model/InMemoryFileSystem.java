package com.vahan.app.model;

import com.google.inject.Inject;
import com.vahan.app.exception.FileContentEmptyException;
import com.vahan.app.exception.FileInvalidRangeException;
import com.vahan.app.exception.FileNameAlreadyExistException;
import com.vahan.app.exception.FileNotFoundException;

import com.vahan.app.factory.VoFactory;
import com.vahan.app.service.FileSystem;
import com.vahan.app.service.FileSystemDB;
import com.vahan.app.vo.File;

public class InMemoryFileSystem implements FileSystem {

    @Inject
    public VoFactory voFactory;

    @Inject
    public FileSystemDB fileSystemDB;


    @Inject
    private InMemoryFileSystem() {

    }

    @Override
    public File create(String name, String content) throws FileNameAlreadyExistException, FileContentEmptyException {
        if (fileSystemDB.fileExists(name)){
            throw new FileNameAlreadyExistException("Given fileName already exists. ");
        }
        if (content == null || content.length() == 0) {
            throw new FileContentEmptyException("File content can't be empty");
        }
        int contentLength = content.length();
        int startIndex = fileSystemDB.getNextFreeSlot(contentLength);
        fileSystemDB.addContentToFileStorage(startIndex, content);
        int endIndex = fileSystemDB.getNextBufferIndex(startIndex+contentLength);
        File file = voFactory.getFile(name, contentLength, startIndex, endIndex);
        fileSystemDB.addFile(file);
        return file;
    }


    @Override
    public String read(String name) {
        //System.out.println(fileSystemDB.getFileMap().toString());
        if (!fileSystemDB.fileExists(name)) {
            return "File not found: " + name;
        } else {
            Integer startIndex =  fileSystemDB.getFile(name).getStartIndex();
            Integer length =  fileSystemDB.getFile(name).getContentLength();
            //String result = String.copyValueOf(fileSystemDB.getFileStorage(), startIndex, length);
            String result = fileSystemDB.readContent(startIndex, length);
            return  result;

        }

    }

    @Override
    public String read(String name, int fromByte, int size) throws FileInvalidRangeException {
        if (!fileSystemDB.fileExists(name) && size > 0) {
            return "File not found: " + name;
        } else {
            Integer startIndex =  fileSystemDB.getFile(name).getStartIndex();
            Integer length =  fileSystemDB.getFile(name).getContentLength();

            if (startIndex+fromByte+size > startIndex+ length + 1 ) {
                throw new FileInvalidRangeException(
                        String.format("Given fromByte %s and size %d combination exceeds file length", fromByte, size));
            }
            //String result = String.copyValueOf(fileSystemDB.getFileStorage(), startIndex+fromByte-1, size);

            String result = fileSystemDB.readContent(startIndex+fromByte-1, size);
            return  result;
        }
    }

    @Override
    public void update(String name, String content) throws FileNameAlreadyExistException, FileNotFoundException, FileContentEmptyException {
        if (!fileSystemDB.fileExists(name)) {
            throw new FileNotFoundException("File not found: " + name);
        }
        if (content == null || content.length() == 0) {
            throw new FileContentEmptyException("File content can't be empty. Not updating file content.");
        }
        delete(name);
        create(name, content);
    }

    @Override
    public void delete(String name) {
        if (fileSystemDB.fileExists(name)) {
            fileSystemDB.deleteFile(name);
        }

    }

}
