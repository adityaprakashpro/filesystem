package com.vahan.app.exception;

public class FileNameAlreadyExistException extends Exception {


    private static final long serialVersionUID = 5955514868983012364L;

    public FileNameAlreadyExistException(String message)  {
        super(message);
    }
}
