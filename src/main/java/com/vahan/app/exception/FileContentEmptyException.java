package com.vahan.app.exception;

public class FileContentEmptyException extends Exception {

    private static final long serialVersionUID = 6404811688299543116L;

    public FileContentEmptyException(String message)  {
        super(message);
    }
}
