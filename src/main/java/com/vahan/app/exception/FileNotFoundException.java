package com.vahan.app.exception;

public class FileNotFoundException extends Exception {

    private static final long serialVersionUID = -8106755164303362514L;

    public FileNotFoundException(String message)  {
        super(message);
    }
}
