package com.vahan.app.exception;

public class FileInvalidRangeException extends Exception {

    private static final long serialVersionUID = 3909918350251627365L;

    public FileInvalidRangeException(String message)  {
        super(message);
    }
}
