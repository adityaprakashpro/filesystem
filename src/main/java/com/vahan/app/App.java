package com.vahan.app;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.vahan.app.injector.AppInjector;
import com.vahan.app.service.FileSystem;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws Exception
    {
        Injector injector = Guice.createInjector(new AppInjector());

        FileSystem fileSystem = injector.getInstance(FileSystem.class);

        fileSystem.create("f1.txt", "file 1");
        fileSystem.create("f2.txt", "This is a large file. Text in file 2. This is a large file. Text in file 2. This is a large file.");

        System.out.println(fileSystem.read("f1.txt"));
        fileSystem.update("f1.txt", "Updated file 1 content");
        System.out.println(fileSystem.read("f1.txt"));
        System.out.println(fileSystem.read("f1.txt", 5, 18));
        System.out.println(fileSystem.read("f2.txt"));


        //fileSystem.delete("f1.txt");
        //fileSystem.update("f1.txt", "");
        // Will throw FileNotFoundException.
        //System.out.println(fileSystem.read("f1.txt"));


        //fileSystem.update("f1.txt", "");
        // Will throw FileContentEmptyException excpetion
        //System.out.println(fileSystem.read("f1.txt"));

        fileSystem.delete("f1.txt");
        System.out.println(fileSystem.read("f1.txt"));

        //fileSystem.update("f1.txt", "file 1");

    }
}
