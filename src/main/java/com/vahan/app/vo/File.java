package com.vahan.app.vo;

public class File {

    String name;

    Integer contentLength;

    Integer startIndex;

    Integer endIndex;

    public File(String name, Integer contentLength, Integer startIndex, Integer endIndex) {
        this.name = name;
        this.contentLength = contentLength;
        this.startIndex  = startIndex;
        this.endIndex = endIndex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Integer getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(Integer startIndex) {
        this.startIndex = startIndex;
    }

    public Integer getEndIndex() {
        return endIndex;
    }

    public void setEndIndex(Integer endIndex) {
        this.endIndex = endIndex;
    }

    public Integer getContentLength() {
        return contentLength;
    }

    public void setContentLength(Integer contentLength) {
        this.contentLength = contentLength;
    }

    @Override
    public String toString() {
        return "File{" +
                "name='" + name + '\'' +
                ", startIndex=" + startIndex +
                ", endIndex=" + endIndex +
                '}';
    }
}
