package com.vahan.app.service;

import com.vahan.app.exception.FileContentEmptyException;
import com.vahan.app.exception.FileInvalidRangeException;
import com.vahan.app.exception.FileNameAlreadyExistException;
import com.vahan.app.exception.FileNotFoundException;
import com.vahan.app.vo.File;

public interface FileSystem {
    //creates a file with given name and content, return a file object
//create an appropriate File class to model a file
    File create(String name, String content) throws FileNameAlreadyExistException, FileContentEmptyException;
    //returns the entire file content as string
    String read(String name);
    //returns size bytes of file as String starting from fromByte
    String read(String name, int fromByte, int size) throws FileInvalidRangeException;
    //updates the file with given content
    void update(String name, String content) throws FileNameAlreadyExistException, FileNotFoundException, FileContentEmptyException;
    //deletes the file
    void delete(String name);
}