package com.vahan.app.service;

import com.vahan.app.vo.File;

public interface FileSystemDB {

    boolean fileExists(String name);

    File getFile(String name);

    void addFile(File file);

    File deleteFile(String name);

    void addContentToFileStorage(int startIndex, String content);

    String readContent(int offset, int size);

    Integer getNextFreeSlot(int contentLength);

    Integer getNextBufferIndex(int offset);


}
