package com.vahan.app.injector;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import com.vahan.app.dao.DefaultFileSystemDB;
import com.vahan.app.factory.VoFactory;
import com.vahan.app.model.InMemoryFileSystem;
import com.vahan.app.service.FileSystem;
import com.vahan.app.service.FileSystemDB;


public class AppInjector extends AbstractModule  {


    @Override
    protected void configure() {
        bind(Integer.class).annotatedWith(Names.named("blockSize")).toInstance(10);
        bind(VoFactory.class);
        bind(FileSystemDB.class).to(DefaultFileSystemDB.class);
        bind(FileSystem.class).to(InMemoryFileSystem.class);
    }
}
