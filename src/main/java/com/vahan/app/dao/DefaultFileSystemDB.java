package com.vahan.app.dao;

import com.google.inject.Inject;
import com.vahan.app.factory.VoFactory;
import com.vahan.app.service.FileSystemDB;
import com.vahan.app.vo.File;

import javax.inject.Named;
import java.util.*;

public class DefaultFileSystemDB implements FileSystemDB{

    private char fileStorage[] ;
    private Map<String, File> fileMap;
    private Integer blockSize;
    private Integer  firstIndex;

    @Inject
    public VoFactory voFactory;

    @Inject
    private DefaultFileSystemDB(@Named("blockSize")Integer blockSize) {
        this.fileStorage =  new char[10000];
        this.fileMap = new HashMap<>();
        this.blockSize = blockSize;
        this.firstIndex = 0;
    }


    public char[] getFileStorage() {
        return fileStorage;
    }


    public Map<String, File> getFileMap() {
        return fileMap;
    }

    public Integer getFirstIndex() {
        return firstIndex;
    }

    public Integer getBlockSize() {
        return blockSize;
    }

    public boolean fileExists(String name) {
        return fileMap.containsKey(name);
    }

    public void addFile(File file) {
        fileMap.put(file.getName(), file);
    }

    public  File getFile(String name) {
        return fileMap.get(name);
    }

    public File deleteFile(String name) {
        return fileMap.remove(name);
    }



    public String readContent(int offset, int length) {
        return String.copyValueOf(getFileStorage(), offset, length);
    }

    public Integer getNextBufferIndex(int index) {
        return index + (getBlockSize() - index % getBlockSize()) - 1;
    }

    public void addContentToFileStorage(int start_index, String content) {
        for (int i=0; i< content.length(); i++) {
            fileStorage[i+start_index] = content.charAt(i);
        }
    }

    public Integer getNextFreeSlot(int contentLength) {
        List<File> files= new ArrayList<>(getFileMap().values());
        if (files.size() == 0) {
            return 0;
        }
        Collections.sort(files, voFactory.getFileComparator());
        int i;
        int gap = files.get(0).getStartIndex() - getFirstIndex();
        if (gap > contentLength && gap >= getBlockSize()) {
            return getFirstIndex();
        }
        for (i=0; i< files.size()-1; i++) {
            if (gap > contentLength && gap >= getBlockSize()) {
                return files.get(i).getEndIndex() + 1;
            }
            gap = files.get(i+1).getStartIndex() - files.get(i).getEndIndex();
        }
        return files.get(i).getEndIndex() + 1;

    }

}
