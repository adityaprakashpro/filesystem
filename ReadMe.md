## FileSystem Project ##

This project uses maven, google guice for Dependency injection.

**Assumptions:**

1. In memory  file system  i    s stored in a char array, it assumes the  char  array will never overflow. The size is set to 
100000 bytes for  now.

2. Will lead to Index out of bounds error not handled this properly when entire file content exceed 10000 chars.

3. For read query of `fromByte` to `size` we need to send the valid range other will lead to  FileInvalidRange error.

4. All file names are considered to be unique.

5. File with empty content can't be created.

6. File are stored in continuous blocks of storage. For example if file that has content of length 15, it is stored from
index 0 till 19, as 10 bytes form a block. 

7. To find a proper block, we find the next most suitable slot where we can fit the file with given content length. 
 
  
**Steps to run the project in MacOS :**

This assumes that the java and maven setup is already present in the client machine.
If not please refer to the link for maven setup  _`https://maven.apache.org/install.html`_.

1. Please check path contains pom.xml

2. mvn clean install

3. mvn exec:java -Dexec.mainClass=com.vahan.app.App  -Dexec.cleanupDaemonThreads=false

This will run the test cases present  in  the main file.

**Things to note :**


1. To increase the block size please change it in AppInjector.

2. Add more senarios to run in com.vahan.app.App class.

3. The FileStorage implementation can be changed easily by implementing FileStorageDB. 


**Things to do better :**

1. If overflowed the fileStroage could have been considered as circular array.
2. Spread the file data across multiple chucks, this can also help us utilize memory.
3. Add unit test cases.


If you any doubts please contact for clarification.